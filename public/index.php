<?php
use HVMP\Model\Group;

include "../vendor/autoload.php";
$app = new \HVMP\App();
/**
 * Route for homepage
 */
$app->get("/", function () {

    $groups = Group::findAllParent();
    $item = \HVMP\Model\Item::findAllWithoutParent();

    $entities = array_merge($item, $groups);
    $addable_groups = Group::findAll();
    include("../app/View/index.php");

});
/**
 * Show detailed page
 */
$app->get("/display/item", function ($params) {
    $item = \HVMP\Model\Item::findById($params->id);
    include("../app/View/item.php");
});

/**
 * Save a new group
 */
$app->post("/group", function ($params) {
    $group = new Group();
    foreach ($params->sub_groups as $sub_group) {
        $sub_group = Group::findById($sub_group);
        if ($sub_group !== false) {
            $group->addEntity($sub_group);
        }
    }
    foreach ($params->sub_items as $sub_item) {
        $sub_item = \HVMP\Model\Item::findById($sub_item);
        if ($sub_item !== false) {
            $group->addEntity($sub_item);
        }
    }
    $group->setName($params->name);
    /** @var PDOStatement $result */
    if ($group->save()) {
        header("Location: /");
    }
});

/*
 * Save a new Item
 */
$app->post("/item", function ($params) {
    $item = new \HVMP\Model\Item();
    foreach ($params->groups as $group) {
        $parent_group = Group::findById($group);
        if ($parent_group !== false) {
            $item->addToGroup($parent_group);
        }
    }

    $item->setName($params->name);
    /** @var PDOStatement $result */
    if ($item->save()) {
        header("Location: /");
    }
});

$app->run();