#HVMP test opdracht
PHP Version = 7.1.1

Stappen die genomen moet worden

* Installeer de composer dependencies
* Maak een database aan "hvmp"
* Voer de migraties uit
* Start the php built in server

```bash
$ composer install
$ echo  "CREATE SCHEMA hvmp" | mysql -uroot -p123
$ php vendor/bin/phinx migrate
$ cd public
$ php -S localhost:8000
```

#Voorbeeld
Rood = Item
Blauw = Groep
![Screenshot_2017-02-08_12-41-44.png](https://bitbucket.org/repo/b87d6n/images/328628559-Screenshot_2017-02-08_12-41-44.png)