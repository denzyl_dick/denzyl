<?php

namespace HVMP\Model;


use PDO;

/**
 * Class Item
 * @author Denyl Dick <denzyl@live.nl>
 * @package HVMP\Model
 */
class Item extends Model
{
    private $name;
    private $id;
    private $groups = [];

    /**
     * Find a specific item
     * @param $sub_item
     * @return Item
     */
    public static function findById($sub_item)
    {
        $statement = parent::getPdo()->prepare("SELECT * FROM `item` WHERE id = :id");
        $statement->bindValue(":id", $sub_item);
        $statement->execute();
        return $statement->fetchObject(Item::class);

    }

    /**
     * Find all items that aren't in a group
     * @return array
     */
    public static function findAllWithoutParent()
    {
        $statement = parent::getPdo()->query("SELECT i.id, name FROM item AS i WHERE i.id NOT IN (SELECT item_id FROM group_item)  ORDER BY i.id DESC ");

        return $statement->fetchAll(PDO::FETCH_CLASS, Item::class);
    }

    public function save(): bool
    {
        $group = parent::getPdo()->prepare("INSERT INTO `item`(name) VALUES(:name)");
        $group->bindValue(":name", $this->name, PDO::PARAM_STR);
        $group->execute();
        $last_id = parent::getPdo()->lastInsertId();
        foreach ($this->groups as $entity) {
            if ($entity instanceof Group) {
                $statement = parent::getPdo()->prepare("INSERT INTO group_item(group_id,item_id)VALUES(:group_id,:item_id)");
                $statement->bindValue(":item_id", $last_id, PDO::PARAM_INT);
                $statement->bindValue(":group_id", $entity->getId(), PDO::PARAM_INT);
                $statement->execute();
            }
        }
        return true;
    }


    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function addToGroup($group)
    {
        $this->groups[] = $group;
    }

    public function getName()
    {
        return $this->name;
    }
}