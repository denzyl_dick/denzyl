<?php

namespace HVMP\Model;


use PDO;

/**
 * Class Group
 * @package HVMP\Model
 * @author Denzyl Dick <denzyl@live.nl>
 */
class Group extends Model
{
    private $id;
    private $name;
    private $sub_entities = [];

    /**
     * Find a specific item
     * @param $sub_item
     * @return Item
     */
    public static function findById($sub_group): Group
    {
        $statement = parent::getPdo()->prepare("SELECT * FROM `group` WHERE id = :id");
        $statement->bindValue(":id", $sub_group);
        $statement->execute();
        return $statement->fetchObject(Group::class);

    }


    public function save(): bool
    {
        if (is_null($this->id)) {
            $group = parent::getPdo()->prepare("INSERT INTO `group`(name) VALUES(:name)");
            $group->bindValue("name", $this->name, PDO::PARAM_STR);
            $group->execute();
            $last_id = parent::getPdo()->lastInsertId();
            foreach ($this->sub_entities as $entity) {
                if ($entity instanceof Group) {
                    $statement = parent::getPdo()->prepare("INSERT INTO sub_group(parent_id,child_id)VALUES(:parent_id,:child_id)");
                    $statement->bindValue(":child_id", $last_id, PDO::PARAM_INT);
                    $statement->bindValue(":parent_id", $entity->getId(), PDO::PARAM_INT);
                    $statement->execute();
                } else if ($entity instanceof Item) {
                    $statement = parent::getPdo()->prepare("INSERT INTO group_item(group_id, item_id)VALUES(:group_id,:item_id)");
                    $statement->bindValue(":group_id", $last_id, PDO::PARAM_INT);
                    $statement->bindValue(":item_id", $entity->getId());
                    $statement->execute();
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Add a sub entity
     * @param Model $entity
     */
    public function addEntity(Model $entity)
    {
        $this->sub_entities[] = $entity;
    }

    /**
     * Find all groups that aren't a child of another group
     * @return array
     */
    public static function findAllParent()
    {
        $statement = parent::getPdo()->query("SELECT g.id, name FROM `group` AS g WHERE g.id NOT IN (SELECT child_id FROM sub_group)  ORDER BY g.id DESC ");

        return $statement->fetchAll(PDO::FETCH_CLASS, Group::class);
    }

    /**
     * Find all group
     * @return array
     */
    public static function findAll()
    {
        $statement = parent::getPdo()->query("SELECT g.id, name FROM `group` AS g   ORDER BY g.id DESC ");

        return $statement->fetchAll(PDO::FETCH_CLASS, Group::class);
    }


    /**
     * Get all items in this group
     * @return array
     */
    public function getItems(): array
    {
        $statement = parent::getPdo()->prepare("SELECT * FROM `item` WHERE `item`.id IN (SELECT item_id FROM group_item WHERE group_id = :group_id )");
        $statement->bindValue(":group_id", $this->id);
        $statement->execute();
        $items = $statement->fetchAll(PDO::FETCH_CLASS, Item::class);
        return $items;
    }

    /**
     * Get all groups in this group
     * @return array
     */
    public function getSubGroups(): array
    {
        $statement = parent::getPdo()->prepare("SELECT * FROM `group` WHERE `group`.id IN (SELECT child_id FROM sub_group WHERE parent_id = :parent_id )");
        $statement->bindValue(":parent_id", $this->id);
        $statement->execute();
        $groups = $statement->fetchAll(PDO::FETCH_CLASS, Group::class);
        return $groups;
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }
}