<?php


namespace HVMP\Model;


use PDO;

/**
 * Class Model
 * @author Denyl Dick <denzyl@live.nl>
 * @package HVMP\Model
 */
abstract class Model
{
    private static $pdo;

    /**
     * Get an instance of PDO. Beware this returns the same instance every time you call it.
     * @return PDO
     */
    public static function getPdo(): PDO
    {
        if (self::$pdo instanceof PDO == false) {
            $dsn = "mysql:host=localhost;dbname=hvmp;";
            $pdo = new PDO($dsn, "root", "123");
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$pdo = $pdo;
        }

        return self::$pdo;
    }

    /**
     * Every model must have the ability to save their data
     * @return bool
     */
    public abstract function save(): bool;
    /**
     * Find a specific model
     * @param $id
     * @return Model
     */
    public static abstract function findById($id);
}