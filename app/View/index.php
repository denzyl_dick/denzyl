<html>
<head>
    <title>HVMP</title>
    <style>
        .entity {

            border: 1px solid;
            margin:10px;
        }

        .group {
            border-color: rgba(0, 0, 255, 0.22);
            background-color: rgba(0, 0, 255, 0.23)
        }

        .item {
            border-color: rgba(255, 0, 94, 0.21);
            background-color: rgba(255, 0, 0, 0.3);
        }

        #form_section {
            border-top: 1px solid #ccc;
        }
    </style>
</head>

<body>
<div>
    <?php
    use HVMP\Model\Group;
    use HVMP\Model\Model;

    foreach ($entities as $entity) {

        displayGroup($entity);
    }
    /**
     * Show every group and items in a recursive manner
     * @param Model $entity
     */
    function displayGroup(Model $entity)
    {
        $class = "";
        $content = "";
        if ($entity instanceof Group) {
            $class = "group";
            $content = "{$entity->getName()}";
        } else if ($entity instanceof \HVMP\Model\Item) {
            $class = "item";
            $content = "<a class='item' href='/display/item?id={$entity->getId()}'/>{$entity->getName()}</a>";
        }

        echo <<<HEAD_GROUP
  <div class='entity {$class}'>
        {$content}  
HEAD_GROUP;
        if ($entity instanceof Group) {
            foreach ($entity->getSubGroups() as $group) {
                displayGroup($group);
            }
            foreach ($entity->getItems() as $item) {
                displayGroup($item);
            }
        }
        echo <<<BOTTOM_GROUP
</div>
BOTTOM_GROUP;


    }

    ?>

</div>
<div id="group_form" style="">
    <form action="/group" method="POST">
        <label for="group_name">Groep naam</label>
        <input type="text" id='group_name' class="form-control" name="name"/>
        <div class="sub_panel">
            <div id="sub_group">
                Deze groep hoort bij?
                <?php
                foreach ($addable_groups as $group) {
                    echo "<div class='input-group'><input type='radio' name='sub_groups[]' value='{$group->getId()}'>{$group->getName()}</div>";
                }
                ?>
            </div>
        </div>
        <input type="submit" class="btn" value="opslaan"/>
    </form>
</div>
<div id="item_form">
    <form action="/item" method="post">
        <label for="item_name">
            Item naam
        </label>
        <input type="text" id="item_name" class="form-control" name="name"/>
        <div>
            Deze item hoort bij?
            <?php
            foreach ($addable_groups as $group) {
                echo "<div class='input-group'><input type='radio' name='groups[]' value='{$group->getId()}'>{$group->getName()}</div>";
            }
            ?>
        </div>
        <input type="submit" class="btn" value="opslaan"/>
    </form>
</div>
</body>
</html>

