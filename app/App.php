<?php

namespace HVMP;
/**
 * This class listen to request and executes the specific callback
 * @version 0.0.1-alpha
 * @author Denzyl Dick <denzyl@live.nl>
 * @package HVMP
 */
class App
{
    private $get_requests;
    private $post_requests;

    public function get($uri, callable $calback)
    {
        $this->get_requests[$uri] = $calback;
    }

    public function post($uri, callable $callback)
    {
        $this->post_requests[$uri] = $callback;
    }

    public function run()
    {
        $request_uri = ($_SERVER['PATH_INFO'] === null ? "/": $_SERVER['PATH_INFO']);
        foreach ($this->get_requests as $uri => $callback) {
            if ($request_uri === $uri) {
                $callback((object)$_GET);
            }
        }

        foreach ($this->post_requests as $uri => $callback) {
            if ($request_uri === $uri) {
                $callback((object)$_POST);
            }
        }
    }
}